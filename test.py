'''Created by Rajat(Delhivery)'''
import pandas as pd
#import numpy as np
#lt=np.random.randint(20,size=20)
#print (lt)
#df=pd.DataFrame({'col':lt})
df=pd.DataFrame.from_csv('out.csv')
print ("mean: ",df.col.mean())
print("median: ",df.col.median())
print("mode: ",df.col.mode())
print("stddev: ",df.col.values.std(ddof=0) )
print("percentile: ",df.col.quantile(0.68))
print("moving Aaverage: \n",pd.rolling_mean(df.col,window=5))
print("moving stddev: \n",pd.rolling_std(df.col,window=5,ddof=0))