---

## About Bootstrap

This project is bootstrap of data team below were various task :

1. Linux and Terminal
2. GIT
3. python
4. Postgres
---

## Linux

All the basic navigation commands along with task given in Assignment 0 is in **week1.pdf** from page 1 to 3.

---

## python

Their are two files one for **assignment 2** i.e. **test1.py** and another for **assigment 3** i.e. **test.py**. Both this python files take **out.csv** as input and perform various task such as finding mean, meadian, mode, percentile, moving average, standard deviation and moving standard deviation.
In **test.py**, pandas 0.17.0 is used if you are using version above than the syntax for moving average and moving standard deviation is different.
All the python code is created under python version 3.5.2

##postgres and psycopg2

Using database *delhivery* and table named *a1* which is populated with *out.csv*. SQl queires for mean, median, mode can be found in **week-1.pdf** page 4. Were as code for **psycopg2** can be found in **test2.py**. In **test2.py** connection is established and records are saved to list.
Also data from **out.csv** is distributed to two tables *a3* and *a4* based on their colomuns, the sql statement uses pipeline and COPY command and can be found in **week-1.pdf** page-4

##general specifications and requirements:

> python version: 3.5.2
> pandas version: 0.17.0
> psycopg2
> database name: "delhivery"
> tables: "a1:" for storing index and col and "a3" for storing index and "a4" for storing col
> out.csv: First column name:"index" type:"integer" desc:"stores index". Second column name:"col" type:"integer" desc:"stores number for input"
