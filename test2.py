import psycopg2
from test1 import *
try:
    connection = psycopg2.connect(user = "delhivery",
                                  password = "welcome@123",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "delhivery")
    cursor = connection.cursor()
     # Print PostgreSQL Connection properties
    print ( connection.get_dsn_parameters(),"\n")
    # Print PostgreSQL version
    #cursor.execute("SELECT version();")
    #record = cursor.fetchone()
    #print("You are connected to - ", record,"\n")
    cursor.execute("select * from a1")    
    record=cursor.fetchall()
    list_1=[item[1] for item in record[0:len(record)]]
    print("List: ",sorted(list_1))
    print("Sum of List: ",sum(list_1))
    print("mean: ",mean(list_1))
    print("median: ",median(list_1))
    print("mode: ",mode(list_1))
    print("standard dev: ",stddev(list_1))
    print("percentile: ",percentile(list_1,.68))
    print("moving_average: ",movavg(list_1,N=5),"\n\n")
    print("Moving_stddev: ",movstd(list_1,N=5,ddof=0),"\n\n")
    #for i in range(3,10):
        #cursor.execute("create table a"+str(i)+"(index serial PRIMARY KEY, col integer NOT NULL);")    
        #cursor.execute("drop table a"+str(i)+";")    
    #connection.commit();    
    print(record)
except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)
finally:
    #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")