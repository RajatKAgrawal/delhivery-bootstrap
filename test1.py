'''Created by Rajat(Delhivery)'''
import csv

def mean(list_new):
    count=len(list_new)
    total=sum(list_new)
    return total/count
    
def median(list_new):
    count=len(list_new)
    if(count<1):
        return None
    if(count%2==1):
        return sorted(list_new)[count//2]
    else:
        return sum(sorted(list_new)[count//2-1:count//2+1])/2.0
        
def mode(list_new):
    if(len(list_new)<1):
        return None
    return max(set(list_new), key=list_new.count)

def sqdev(list_new):
    m=mean(list_new)
    ss=sum((x-m)**2 for x in list_new)
    return ss

def stddev(list_new,ddof=0):
    n=len(list_new)
    if(n<2):
        return None
    ss=sqdev(list_new)
    pvar=ss/(n-ddof)
    return pvar**0.5

def movavg(list_new,N=5):
    cumsum, moving_avg=[0],[]
    for i,x in enumerate(list_new, 1):
        cumsum.append(cumsum[i-1] + x)
        if i>=N:
            moving_ave = (cumsum[i] - cumsum[i-N])/N
            moving_avg.append(moving_ave)
        else:
            moving_avg.append(None)
    return moving_avg

def movstd(list_new,N=5,ddof=0):
    moving_std=[]
    for i,x in enumerate(list_new, 1):
        if i>=N:
            list_temp=list_new[i-N:i]
            #print("temp list[",i,"]: ",list_temp)
            moving_std.append(stddev(list_temp,ddof))
        else:
            moving_std.append(None)
    return moving_std

'''def percentile(list_new,percent=0.72):
    n=len(list_new)
    return sorted(list_new)[int(math.ceil(n*percent))-1]'''

def percentile(list_new ,percent=0.5):
    list_new=sorted(list_new)
    ind=((float(len(list_new))-1.0)*percent)
    ans=(list_new[int(ind)]*(1.0-(ind-int(ind))))+(list_new[int(ind)+1]*((ind-int(ind))))
    return ans
def main():    
    with open('out.csv') as csv_file:
        csv_reader=csv.reader(csv_file,delimiter=',')
        csv_list=list(csv_reader)   
    #list_1=[(csv_list[i][1:2]) for i in range(1,len(csv_list))]
    list_1=[item[1] for item in csv_list[1:len(csv_list)]]
    list_new=list(map(int,list_1))
    print("List: ",sorted(list_new))
    print("Sum of List: ",sum(list_new))
    print("mean: ",mean(list_new))
    print("median: ",median(list_new))
    print("mode: ",mode(list_new))
    print("standard dev: ",stddev(list_new))
    print("percentile: ",percentile(list_new,.68))
    print("moving_average: ",movavg(list_new,N=5))
    print("Moving_stddev: ",movstd(list_new,N=5,ddof=0))
if __name__=="__main__":
    main()